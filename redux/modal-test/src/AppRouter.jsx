import { Routes, Route } from "react-router-dom";
import HomeRoute from "./routers/HomeRoute";
import CartRoute from "./routers/CartRoute";
import SelectedRoute from "./routers/SelectedRoute";
import PropTypes from 'prop-types';

const AppRouter = ({
    products = [],
    addToСart = () => { },
    cart = [],
    decrementCartItem = () =>{},
    incrementCartItem = () => { },
    deleteCartItem = () => { },
    handleFavourite = () => { },
}) => {
    return (
        <Routes>

            <Route path="/" element={<HomeRoute
                addToСart={addToСart}
                products={products}
                handleFavourite={handleFavourite} />} />
            
            <Route path="/selected" element={<SelectedRoute
                handleFavourite={handleFavourite }
                products={products}
                addToСart={addToСart} />} />
            
            <Route path="/cart" element={<CartRoute
                cart={cart}
                decrementCartItem={decrementCartItem}
                incrementCartItem={incrementCartItem}
                deleteCartItem={deleteCartItem}
            />} />
        </Routes>
    )
}

AppRouter.propTypes = {
    products: PropTypes.array,
    cart: PropTypes.array,
    addToCart: PropTypes.func,
    decrementCartItem: PropTypes.func,
    incrementCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
};

export default AppRouter;