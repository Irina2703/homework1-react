import { configureStore } from '@reduxjs/toolkit';
import productsSlice from './slices/productsSlice';
import logger from 'redux-logger';



const store = configureStore({
    reducer: {
        products: productsSlice,

    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});

export default store;