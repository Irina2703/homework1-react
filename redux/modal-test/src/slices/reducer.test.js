import { expect } from '@jest/globals';
import productReducer from './productsSlice';

describe('productReducer works', () => {
    test('should productReducer  add to cart', () => {
        const state = {
            data: [],
            cart: [],
        };
        const action = {
            type: 'products/addToCartR',
            payload: {
                "id": 2,
                "title": "Огірки",
                "price": 29,
                "image": "https://thefreshdirect.com.au/wp-content/uploads/2023/06/Mini-Cucomber-Qukes-The-fresh-direct-2.webp",
                "sku": "789012",
                "color": "Свіжі та хрусткі огірки"
            },
        };

        expect(productReducer(state, action)).toEqual({
            data: [],
            cart: [{
                "id": 2,
                "count": 1,
                "title": "Огірки",
                "price": 29,
                "image": "https://thefreshdirect.com.au/wp-content/uploads/2023/06/Mini-Cucomber-Qukes-The-fresh-direct-2.webp",
                "sku": "789012",
                "color": "Свіжі та хрусткі огірки"
            }],
        })
    });

    test('should productReducer decrement ', () => {
        const state = {
            data: [],
            cart: [{
                "id": 2,
                "count": 2,
                "title": "Огірки",
                "price": 29,
                "image": "https://thefreshdirect.com.au/wp-content/uploads/2023/06/Mini-Cucomber-Qukes-The-fresh-direct-2.webp",
                "sku": "789012",
                "color": "Свіжі та хрусткі огірки"
            }], };
        const action = {
            type: 'products/decrementCartItemR',
            payload: 2
        }

        expect(productReducer(state, action)).toEqual({ 
            data: [],
            cart: [{
                "id": 2,
                "count": 1, 
                "title": "Огірки",
                "price": 29,
                "image": "https://thefreshdirect.com.au/wp-content/uploads/2023/06/Mini-Cucomber-Qukes-The-fresh-direct-2.webp",
                "sku": "789012",
                "color": "Свіжі та хрусткі огірки"
            }],
    
})
    });


   
});
