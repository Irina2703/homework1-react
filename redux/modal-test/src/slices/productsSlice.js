import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';


export const fetchAllProducts = createAsyncThunk(
    'products/fetchAllProducts',
    async () => {
        const { data } = await axios.get('./products.json');
        return data;
    }
);

const initialState = {
    data: [],
    cart:[],
};



export const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        handleFavouriteR: (state, action) => {
            const product = state.data.find(product => action.payload === product.id);
            if (product) {
                product.isFavourite = !product.isFavourite;
            }
        },   
        
        addToCartR: (state, action) => {
            
            const cartItem = state.cart.find(product => action.payload.id === product.id);
            console.log(cartItem)
            if (!cartItem) {
                state.cart.push({ ...action.payload, count: 1 });
            } else {
                cartItem.count++;
            }
            localStorage.setItem('cart', JSON.stringify(state.cart));
   
        },
        addAllCart: (state, action) => {
            state.cart = action.payload;
            localStorage.setItem('cart', JSON.stringify(state.cart));
        },
        decrementCartItemR: (state, action) => {
            const cartItem = state.cart.find(({ id }) => id === action.payload);
              if (cartItem.count > 1) {
                cartItem.count--;
                  localStorage.setItem('cart', JSON.stringify(state.cart));
              }

        },
        
        incrementCartItemR: (state, action) => {
            const cartItem = state.cart.find(({ id }) => id === action.payload);
                cartItem.count++;
                localStorage.setItem('cart', JSON.stringify(state.cart));
            
        },
        deleteCartItemR: (state, action) => {
            const itemIndex = state.cart.findIndex(item => item.id === action.payload);

            state.cart.splice(itemIndex, 1);
        }
    },

     

    extraReducers: (builder) => {
        builder.addCase(fetchAllProducts.fulfilled, (state, action) => {
            state.data = action.payload;
        });
    }
});

export const { handleFavouriteR, addToCartR, addAllCart, decrementCartItemR, incrementCartItemR, deleteCartItemR } = productsSlice.actions;

export default productsSlice.reducer;
