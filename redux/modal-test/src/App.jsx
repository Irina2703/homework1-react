import React, { useEffect } from 'react';
import './App.css';
import { useImmer } from 'use-immer';
import Header from './components/Header';
import AppRouter from './AppRouter.jsx';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchAllProducts, handleFavouriteR, addToCartR, addAllCart,
  incrementCartItemR, decrementCartItemR, deleteCartItemR
} from './slices/productsSlice.js';





function App() {
  
  const dispatch = useDispatch();
 

  const [products, setProducts] = useImmer([]);
  
 

  const cart = useSelector(state => state.products.cart)

  const cartTotalCount = cart?.reduce((acc, el) => acc + el.count || 0, 0)
  


  const addToСart = (product) => {
    
    dispatch(addToCartR(product));
   
  }


  const decrementCartItem = (itemID) => {
    

    dispatch(decrementCartItemR(itemID));
  };
    
  const incrementCartItem = (itemID) => {
    
    dispatch(incrementCartItemR(itemID));
  };

  const deleteCartItem = (itemID) => {
    
    dispatch(deleteCartItemR(itemID));

  };

  useEffect(() => {
    const cartLocalStorage = localStorage.getItem('cart')
    dispatch(fetchAllProducts());
    if (cartLocalStorage) {
      dispatch(addAllCart(JSON.parse(cartLocalStorage))

      )
    }
      
      
  }, [])

  

  const handleFavourite = (id) => {
  
    dispatch(handleFavouriteR(id));
    
  };




  return (
    <>
      <Header cartTotalCount={cartTotalCount}
        

      />
      
      
      <main>
        <AppRouter
          products={products}
          addToСart={addToСart}
          cart={cart}
          decrementCartItem={decrementCartItem}
          incrementCartItem={incrementCartItem}
          deleteCartItem={deleteCartItem}
          handleFavourite={handleFavourite}
        />
      </main>
    </>
    
        
    
  );
    
}
export default App;



