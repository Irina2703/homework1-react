import PropTypes from 'prop-types';
import CartItem from '../../components/CartItem';
import styles from './CartRoute.module.scss';
import SignUpForm from '../../components/SignUpForm'

const CartRoute = ({ cart = [],
     decrementCartItem = () => { },
    incrementCartItem = () => { },
    deleteCartItem = () => { },
}) => {
    const totalPrice = cart.reduce((acc, el) => acc + el.price * el.count, 0)
    return (
        <>
            <h1>Cart</h1>
            <div className={styles.container}>
                {cart.map(item => <CartItem
                    key={item.id}
                    item={item}
                    decrementCartItem={decrementCartItem}
                    incrementCartItem={incrementCartItem}
                    deleteCartItem= { deleteCartItem}
                />)}
            </div>
           <SignUpForm/>
            <h2> TOTAL PRICE {totalPrice}</h2>
          
        </>
    )
}
CartRoute.propTypes = {
    cart: PropTypes.array,
     decrementCartItem: PropTypes.func,
    incrementCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
}
export default CartRoute;


