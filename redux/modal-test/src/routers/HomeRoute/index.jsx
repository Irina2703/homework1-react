import PropTypes from 'prop-types';
import CardsContainer from '../../components/CardContainer';
import { useSelector } from 'react-redux';


const HomeRoute = ({ products = [], addToСart = () => { }, handleFavourite = () => { }, }) => {
   
    const selectedProducts = useSelector(state => state.products.data)
    return (
        <>
            <h1>PRODUCTS</h1>

            <CardsContainer products={selectedProducts}
                addToСart={addToСart}
               handleFavourite={handleFavourite}
            />
           
        </>
    )
}

HomeRoute.propTypes = {
    products: PropTypes.array,
    addToСart: PropTypes.func,
    handleFavourite: PropTypes.func,
};

export default HomeRoute;