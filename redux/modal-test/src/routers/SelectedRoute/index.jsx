import PropTypes from 'prop-types';
import CardsContainer from '../../components/CardContainer';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';





const SelectedRoute = ({
    handleFavourite = () => { },
    addToСart = () => { }, }

) => {
    
    const selectedProducts = useSelector(state => state.products.data)
    const favouritePoducts = selectedProducts.filter((product) => {
        if (product.isFavourite) {
            console.log(favouritePoducts)
            return true
        } else {
            return false
        }
    })
    //  useEffect(()=> {
    //      localStorage.setItem('favouritePoducts', JSON.stringify (favouritePoducts) )
    //  }, [selectedProducts])

     const totalSelected = favouritePoducts.length;
    

    return (
        <>
            <h1>Selected</h1>
            <p>Total selected: {totalSelected}</p>
            <CardsContainer products={favouritePoducts}
                handleFavourite={handleFavourite}
                addToСart={addToСart}

            />
        </>
    )
}



SelectedRoute.propTypes = {
    products: PropTypes.array,
    handleFavourite: PropTypes.func,

};
export default SelectedRoute;