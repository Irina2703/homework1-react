import PropTypes from 'prop-types';
import styles from './Body.module.scss'

const ModalBody = ({ children }) => {
     
    return (
        <div className={styles.body}>  {children} </div>
    );
};

ModalBody.propTypes = {
    children: PropTypes.node,
};

export default ModalBody;