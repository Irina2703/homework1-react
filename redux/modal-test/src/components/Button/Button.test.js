import Button from '.';
import { render, screen, fireEvent } from '@testing-library/react';

describe("Button snapshot testing", () => {
    test('should Button render', () => {
        const { asFragment } = render(<Button className="some-btn">Hello</Button>);

        expect(asFragment()).toMatchSnapshot();
    });

});




describe('Button Tests', () => {
    test('should onClick works', () => {
        const callback = jest.fn();
        render(<Button onClick={callback}>Hello</Button>);

        const btn = screen.getByText('Hello');

        fireEvent.click(btn);
        // screen.debug(); 

        expect(callback).toBeCalled();
    });
    test('should onClick works', () => {
        
        render(<Button>Hello</Button>);

        const btn = screen.getByText('Hello');

    
        expect(btn).toHaveTextContent("Hello");
    })
});
