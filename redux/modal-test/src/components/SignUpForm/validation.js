import * as Yup from 'yup';
import { isPossiblePhoneNumber } from 'libphonenumber-js'
/*
name
surname
age
adress
mobile

*/
export const signupSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    surname: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    age: Yup.number()
        .min(1, 'Age should be more then 1!')
        .max(99, 'Age should be less then 99!')
        .required('Required')
        .integer('Age should be integer'),
    mobile: Yup.string()
        .required('Required')
        .test('valid-mobile', 'Please enter a valid phone number', (value) => {
           
            return isPossiblePhoneNumber(value, 'US');
        })
       
        .matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/, 'Phone number must be in the format XXX-XXX-XXXX'), // Example
    adress: Yup.string()
        .required('Required'),

});
