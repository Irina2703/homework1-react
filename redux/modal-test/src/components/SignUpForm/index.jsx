import { Formik, Form, Field, ErrorMessage } from 'formik';
import CustomInput from '../CustomInput';
// import { PatternFormat } from 'react-number-format';
import { signupSchema } from './validation';
import Button from '../Button';
import styles from './SignUpForm.module.scss'
import { useDispatch, useSelector } from 'react-redux';
import { addAllCart } from '../../slices/productsSlice';


function SignUpForm() {
    const cart = useSelector((state) => (state.products.cart));

    const dispatch = useDispatch();
    
    const initialValues = {
        name: '',
        surname: '',
        age: '',
        adress: '',
        mobile: '',
       
    }
    const onSubmit = (values, { resetForm }) => {
        console.log(cart)
        dispatch(addAllCart([]))
    console.group("Інформація про користувача!");
        console.log(values);
        console.groupEnd();
        
        
        resetForm()


}

    return (
        <>
            <Formik initialValues= {initialValues} onSubmit={onSubmit} validationSchema={signupSchema}>
                <Form className={styles.container}>
                    <CustomInput type="text" placeholder="Enter your name" name="name" />
                    <CustomInput type="text" placeholder="Enter your surname" name="surname" />
                    <CustomInput type="text" placeholder="Enter your age" name="age" />
                    <CustomInput type="text" placeholder="Enter your adress" name="adress" />
                    <CustomInput type="text" placeholder="Enter your mobile" name="mobile" />
                    <Button type='submit'>Checkout</Button>
                </Form>
            </Formik>
        </>
    )
}
export default SignUpForm