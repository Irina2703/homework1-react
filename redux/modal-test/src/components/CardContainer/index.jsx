import PropTypes from 'prop-types';
import React from 'react';
import ProductCard from '../Product-card';
import styles from './CardContainer.module.scss';


const CardsContainer = ({ products = [],
    handleFavourite = () => { },
    addToСart = () => { } }) => {
    
    

    return (
        <div className={styles.container}>
            {products && products.map((product) => {
                const { title, price, id, color, image, isFavourite } = product

                return <ProductCard key={id} title={title} price={price}
                    id={id} color={color} image={image}
                    isFavourite={isFavourite} handleFavourite={handleFavourite}
                    addToСart={addToСart} product={product}
                />
            })}

        </div>

    )
}
CardsContainer.propTypes = {
    products: PropTypes.array,
    handleFavourite: PropTypes.func,
    addToCart: PropTypes.func,
};

export default CardsContainer;