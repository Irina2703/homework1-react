import PropTypes from 'prop-types';
import React from 'react';
import ModalWrapper from '../ModalWrapper';
import Modal from '../Modal';
import ModalHeader from '../Modal-header';
import ModalBody from '../ModalBody';
import ModalFooter from '../Modal-footer';
import ModalClose from '../Modal-close';





const ModalWindowOne = ({ handleCloseFirstModal,
    image,
    title,
     deleteCartItem = () => { },
    
}) => {
    const handleModalClick = (e) => {
        if (e.target.classList.contains('background')) {
            handleCloseFirstModal();
        }
    };
    

    

    return (
        <ModalWrapper>
            <div className="background" onClick={handleModalClick}>
                <Modal>
                    <div height='140px'> <img src={image} alt='text' /></div>
                    <ModalHeader>
                        <h2> {title} Delete!</h2>
                    </ModalHeader>
                    <ModalBody>
                        <p>By clicking the “Yes, Delete” button, PRODUCT {title} will be deleted.</p>
                    </ModalBody>
                    <ModalFooter
                        firstText="No,Cancel"
                        firstClick={handleCloseFirstModal}
                        secondaryText="Yes, Delete"
                        secondaryClick={deleteCartItem}
                    />
                    <ModalClose onClick={handleCloseFirstModal} />
                </Modal>
            </div>
        </ModalWrapper>
    );
};

ModalWindowOne.propTypes = {
    handleCloseFirstModal: PropTypes.func.isRequired,
    deleteCartItem:PropTypes.func,
};

export default ModalWindowOne;