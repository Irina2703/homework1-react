import PropTypes from 'prop-types';
import styles from './Header.module.scss'
import { NavLink } from "react-router-dom";
import SelectedIcon from '../SelectedIcon';
import { useSelector } from 'react-redux';

const Header = ({ children, cartTotalCount = 0,  }) => {
    const favourites = useSelector(state => state.products.data);
    const favouritesLength = favourites.filter(product => product.isFavourite).length;
    
    return (
        <div className={styles.header}>
            <nav className={styles.headerNavigation}>
                <ul>
                    <li>
                        <NavLink end to="/" className={(({ isActive }) => isActive && styles.active)}>Home</NavLink>
                    </li>
                </ul>
                <ul >
                    <li >
                        <NavLink end to="/selected" className={(({ isActive }) => isActive && styles.active)}>
                            <SelectedIcon /> 
                            <span>{favouritesLength}</span>
                        </NavLink>
                        
                    </li>
                </ul>
                <nav>
                    <ul>
                        <li>
                            < NavLink end to="/cart" className={(({ isActive }) => isActive && styles.active)}>
                               
                                    
                                
                                {<img src='./img/shoppingcart.png' alt='Cart' className={styles.img} />}
                                <span>{cartTotalCount} </span>
                            </NavLink>
                        </li>
                    </ul>
                </nav>
            
                {children}
            </nav>
        </div>
    );
};

Header.propTypes = {
    children: PropTypes.node,
    
};


export default Header;