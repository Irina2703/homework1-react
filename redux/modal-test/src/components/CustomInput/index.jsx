
import { useField } from 'formik';
import styles from './CustomInput.module.scss'

function CustomInput(props) {
    const { type, placeholder } = props;
    const [field, meta] = useField(props);

    const isError = meta.touched && meta.error;

    
         return (
        <div className={styles.container}>
            <input style={{ borderColor: isError ? 'red' : 'initial' }} className={styles.input} type={type} placeholder={placeholder} {...field} />
            {isError && <p className={styles.error}>{meta.error}</p>}
        </div>
    )

}

export default CustomInput