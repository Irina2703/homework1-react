import Modal from '.';
import { render, screen, fireEvent } from '@testing-library/react';

describe("Modal snapshot testing", () => {
    test('should Modal render', () => {
        const { asFragment } = render(<Modal>Hello</Modal>);

        expect(asFragment()).toMatchSnapshot();
    });

});
