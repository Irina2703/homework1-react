import PropTypes from 'prop-types';
import CardsContainer from '../../components/CardContainer';

const SelectedRoute = ({ products = [], handleFavourite = () => { },
    addToСart = () => { }, }

) => {

    const favouritePoducts = products.filter((product) => {
        if (product.isFavourite) {
            return true
        } else {
            return false
        }
})

    return (
        <>
            <h1>Selected</h1>

            <CardsContainer products={favouritePoducts}
                handleFavourite={handleFavourite}
                addToСart={addToСart}

            />
        </>
    )
}



SelectedRoute.propTypes = {
    products: PropTypes.array,
    handleFavourite: PropTypes.func,

};
export default SelectedRoute;