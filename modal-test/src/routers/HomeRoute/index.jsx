import PropTypes from 'prop-types';
import CardsContainer from '../../components/CardContainer';

const HomeRoute = ({ products = [], addToСart = () => { }, handleFavourite = () => { }, }) => {
    console.log(addToСart);
    return (
        <>
            <h1>PRODUCTS</h1>

            <CardsContainer products={products}
                addToСart={addToСart}
               handleFavourite={handleFavourite}
            />
        </>
    )
}

HomeRoute.propTypes = {
    products: PropTypes.array,
    addToСart: PropTypes.func,
    handleFavourite: PropTypes.func,
};

export default HomeRoute;