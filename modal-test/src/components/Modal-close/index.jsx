import PropTypes from 'prop-types';
import React from 'react';
import styles from './Close.module.scss';

const ModalClose = ({ onClick = () => { }} ) => {

    return (
        <button className={styles.close} onClick={onClick}>X</button>
    );
};

ModalClose.propTypes = {
    onClick: PropTypes.func,
};

export default ModalClose;