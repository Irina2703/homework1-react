import PropTypes from 'prop-types';
import React from 'react'
import styles from './Modal.module.scss';

const Modal = ({ children }) => {

    return (
        <div className={styles.modal}>
            {children}
        </div>
    );
};

Modal.propTypes = {
    children: PropTypes.node,
};
    


export default Modal;
