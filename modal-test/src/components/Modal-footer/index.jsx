import PropTypes from 'prop-types';
import React from 'react';
import Button from '../Button';
import styles from'./Footer.module.scss'

const ModalFooter = ({ firstText = ' ', secondaryText = ' ', firstClick, secondaryClick }) => {

    return (
        <div className={styles.footer}>
            {firstText && firstClick &&
                <Button onClick={firstClick}>{firstText}</Button>
            }
            {secondaryText && secondaryClick &&
                <Button onClick={secondaryClick}>{secondaryText}</Button>
            }
        </div>
    );
};

ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func,
};



export default ModalFooter;