
import ModalWrapper from '../ModalWrapper';
import Modal from '../Modal';
import ModalHeader from '../Modal-header';
import ModalBody from '../ModalBody';
import ModalFooter from '../Modal-footer';
import ModalClose from '../Modal-close';
import PropTypes from 'prop-types';



const ModalWindowSecond = ({ handleCloseSecondModal,
    product = {},
    title,
    price,
    addToСart = () => { } }) => {
    
    const handleModalClickSecond = (e) => {
        if (e.target.classList.contains('background')) {
            handleCloseSecondModal();
        }
    };

    const handleAddToCart = () => {
        addToСart(product);
        handleCloseSecondModal();
    };
    
    return (
        <ModalWrapper>
            <div className="background" onClick={handleModalClickSecond}>
            <Modal>
                <ModalHeader>
                        <h2>Add Product {title }</h2>
                </ModalHeader>

                <ModalBody>
                        <p>Price: {price}</p>
                </ModalBody>

                <ModalFooter
                   
                        firstText="ADD TO CART"
                        firstClick={handleAddToCart}
                />
                <ModalClose onClick={handleCloseSecondModal} />
            </Modal>
            </div>
        </ModalWrapper>

    );

};

ModalWindowSecond.propTypes = {
    handleCloseSecondModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func,
}

export default ModalWindowSecond;