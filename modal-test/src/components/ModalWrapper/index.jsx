import PropTypes from 'prop-types';
import React from 'react';
import styles from './Wrapper.module.scss';

const ModalWrapper = ({ children }) => {

    return (
        <div className={styles.wrapper}>
            {children}
        </div>
    );
};

ModalWrapper.propTypes = {
    children: PropTypes.node
};

export default ModalWrapper;