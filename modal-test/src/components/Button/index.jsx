import React from 'react'
import PropTypes from 'prop-types';
import styles from './Button.module.scss'

const Button = ({ type, classNames, onClick = () => {}, children }) => {
    
    return (
        <button type={type} className={styles.button} onClick={onClick} >
            {children}
        </button>
    );
}; 
Button.propTypes = {
    type: PropTypes.string,
    classNames: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node,  
}
export default Button;