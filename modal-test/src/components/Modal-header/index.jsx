import PropTypes from 'prop-types';
import styles from './HeaderModal.module.scss'

const ModalHeader = ({ children }) => {

    return (
        <div className={styles.headermodal}>
            {children}
        </div>
    );
};

ModalHeader.propTypes = {
    children: PropTypes.node
};


export default ModalHeader;