import PropTypes from 'prop-types';
import React, { useState } from 'react';
import styles from './CartItem.module.scss';
import Button from '../Button';
import ModalWindowOne from '../Modal-window1';

function CartItem({
    item = {},
    decrementCartItem = () => { },
    incrementCartItem = () => { },
    deleteCartItem = () => { },
}) {
    const [firstModalOpen, setFirstModalOpen] = useState(false);

    const handleFirstModalOpen = () => {
      setFirstModalOpen(item.id);
    };

    const handleCloseFirstModal = () => {
      setFirstModalOpen(false);
    };

    const totalPrice = item.price * item.count;

    return (
        <div className={styles.item}>
            <img src={item.image} alt={item.name} />
            <h2 className={styles.productName}>{item.name}</h2>
            <p className={styles.productPrice}>Price: {item.price}</p>

            <p className={styles.count}>Count: {item.count}</p>


            <p className={styles.count}>Total: {totalPrice}</p>

            <Button onClick={() => { decrementCartItem(item.id) }}>-</Button>
            <Button onClick={() => { incrementCartItem(item.id) }}>+</Button>
            <Button onClick={handleFirstModalOpen}>Delete</Button>
            {firstModalOpen === item.id &&  (
                <ModalWindowOne
                    handleCloseFirstModal={handleCloseFirstModal}
                    title={item.title}
                    image={item.image}
                    deleteCartItem={() => { deleteCartItem(item.id) }}
                    
                />)}
            
        </div>
    );
}

CartItem.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string,
        image: PropTypes.string,
        price: PropTypes.number,
        count: PropTypes.number,
    }),
    addToCart: PropTypes.func,
    decrementCartItem: PropTypes.func,
    incrementCartItem: PropTypes.func,
    deleteCartItem: PropTypes.func,
};

export default CartItem;
