import PropTypes from 'prop-types';
import styles from './ProductCard.module.scss'
import classNames from 'classnames';
import Button from '../Button';
import ModalWindowSecond from '../Modal-window2'
import React, { useState } from 'react';
import StarSvg from '../Starsvg'

const ProductCard = ({
    product = {},
    title = " ",
    id,
    price = " ",
    color = " ",
    image,
    handleFavourite = () => { },
    isFavourite = false,
    addToСart = () => { },
    


}) => {
    const [secondModalOpen, setSecondModalOpen] = useState(false);

    const handleSecondModalOpen = () => {
        setSecondModalOpen(true);
    };

    const handleCloseSecondModal = () => {
        setSecondModalOpen(false);
    };

    
    return (
        <div className={styles.card}>
            <div 
                className={classNames(styles.svgWrapper, { [styles.active]: isFavourite })}
                onClick={() => { handleFavourite(id) }}
            >
                <StarSvg />
            </div>

            <img src={image} alt="Some text" className={styles.image} />
            <h2 className={styles.title}>{title}</h2>

            <p className={styles.price}> Price:  {price} UAH</p>
            <span className={styles.color}>  {color}</span>

            <Button onClick={handleSecondModalOpen}>Add to cart</Button>
            {secondModalOpen && (
                <ModalWindowSecond
                    handleCloseSecondModal={handleCloseSecondModal}
                    title={title}
                    price={price}
                    product={product}
                    addToСart={addToСart}
                />)}





        </div>
    )
}


ProductCard.propTypes = {
    title: PropTypes.string,
    id: PropTypes.number.isRequired,
    price: PropTypes.number,
    color: PropTypes.string,
    image: PropTypes.string,
    handleFavourite: PropTypes.func,
    isFavourite: PropTypes.bool,
    addToCart: PropTypes.func,

};

export default ProductCard;