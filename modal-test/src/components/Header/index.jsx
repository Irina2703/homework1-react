import PropTypes from 'prop-types';
import styles from './Header.module.scss'
import { NavLink } from "react-router-dom";
import CartIcon from '../Carticon'
import SelectedIcon from '../SelectedIcon';

const Header = ({ children, cartTotalCount = 0 }) => {

    return (
        <div className={styles.header}>
            <nav className={styles.headerNavigation}>
                <ul>
                    <li>
                        <NavLink end to="/" className={(({ isActive }) => isActive && styles.active)}>Home</NavLink>
                    </li>
                </ul>
                <ul>
                    <li>
                        <NavLink end to="/selected" className={(({ isActive }) => isActive && styles.active)}>
                            <SelectedIcon />
                            {/* <span>{selectedTotal }</span> */}
                        </NavLink>
                        
                    </li>
                </ul>
                <nav>
                    <ul>
                        <li>
                            < NavLink end to="/cart" className={(({ isActive }) => isActive && styles.active)}>
                               
                                    {/* <CartIcon/> */}
                                
                                {<img src='./img/shoppingcart.png' alt='Cart' className={styles.img} />}
                                <span>{cartTotalCount} </span>
                            </NavLink>
                        </li>
                    </ul>
                </nav>
            
                {children}
            </nav>
        </div>
    );
};

Header.propTypes = {
    children: PropTypes.node
};


export default Header;