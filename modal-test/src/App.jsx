import React, { useEffect } from 'react';
import './App.css';
import { useImmer } from 'use-immer';
import axios from 'axios';
import Header from './components/Header';
import AppRouter from './AppRouter.jsx';
import { produce } from 'immer';





function App() {
  

  const [products, setProducts] = useImmer([]);
  const [cart, setCart] = useImmer([]);
  // const [selected, setSelected] = useImmer([]);

  const cartTotalCount = cart?.reduce((acc, el) => acc + el.count || 0, 0)
  // const selectedTotal = selected?.reduce((acc, el) => acc + el.count || 0, 0)

  const addToСart = (product) => {
    
    setCart(produce(draft => {
      const cartItem = draft.find(({ id }) => id === product.id)
      if (!cartItem) {
        draft.push({ ...product, count: 1 });
      } else {
        cartItem.count++;
      }
      console.log(cartItem)
      

      localStorage.setItem('cart', JSON.stringify(draft));
    }));
   
  }


  const decrementCartItem = (itemID) => {
    setCart(draft => {
      const cartItem = draft.find(({ id }) => id === itemID);
      if (cartItem.count > 1) {
        cartItem.count--;
        localStorage.setItem('cart', JSON.stringify(draft));
      }
      
    })
  };
    
  const incrementCartItem = (itemID) => {
    setCart(draft => {
      const cartItem = draft.find(({ id }) => id === itemID);
      cartItem.count++;
      localStorage.setItem('cart', JSON.stringify(draft));
    })
  };

  const deleteCartItem = (itemID) => {
    setCart(draft => {
      const itemIndex = draft.findIndex(item => item.id === itemID);

      draft.splice(itemIndex, 1);

    });
  };

  useEffect(() => {
    const cartLocalStorage = localStorage.getItem('cart')
    
    if (cartLocalStorage) {
      setCart(JSON.parse(cartLocalStorage)

      )
    }
      
      
  }, [])

  useEffect(() => {
    async function FetchProducts() {
      try {
        const { data } = await axios.get('./products.json');
        setProducts(data);
        console.log(data);
      } catch (error) {
        console.error('Error message');
        
      }
    }

    FetchProducts();
  }, []);

  const handleFavourite = (id) => {
    setProducts(produce(draft => {
      const product = draft.find(product => id === product.id);
      if (product) {
        product.isFavourite = !product.isFavourite;
      }
    }));
  };




  return (
    <>
      <Header cartTotalCount={cartTotalCount}
        // selectedTotal={selectedTotal}

      />
      
      
      <main>
        <AppRouter
          products={products}
          addToСart={addToСart}
          cart={cart}
          decrementCartItem={decrementCartItem}
          incrementCartItem={incrementCartItem}
          deleteCartItem={deleteCartItem}
          handleFavourite={handleFavourite}
        />
      </main>
    </>
    
        
    
  );
    
}
export default App;


